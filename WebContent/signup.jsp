<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">

			<label for="loginId">ログインID</label> <input name="loginId"
				value="${inputUser.loginId}" id="loginId" /><br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /> <br /> <label for="confirmPass">再確認パスワード</label> <input
				name="confirmPass" type="password" id="confirmPass" /> <br /> <label
				for="name">名前</label> <input name="name" value="${inputUser.name}"
				id="name" /> <br /> <label for="branch">支店名</label> <select
				name="branch" id="branch">

				<c:forEach items="${branch}" var="branch">
					<c:choose>
						<c:when test="${branch.id==inputUser.branch}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${branch.id}">${branch.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br /> <label for="position">部署・役職</label> <select name="position"
				id="position">
				<c:forEach items="${position}" var="position">
					<c:choose>
						<c:when test="${position.id==inputUser.position}">
							<option value="${position.id}" selected>${position.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${position.id}">&{position.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
	</div>
</body>
</html>

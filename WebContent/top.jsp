<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html　PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<h3>掲示板システム( ..)φ</h3>

<body>
	<div class="main-contents">
		<div class="header">
			<a href="signup">新規登録</a>
		</div>
	</div>
	
	<table>
		<tr>
			<th>ID</th>
			<th>名前</th>
			<th>支店</th>
			<th>部署・役職</th>
		</tr>
		　<c:forEach var="user" items="${user}">
			<tr>
				<td><c:out value="${user.loginId }" /></td>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.branch_name }" /></td>
				<td><c:out value="${user.position_name }" /></td>
				<td><a href= "setting?id=${user.id}">編集する</a></td>
			</tr>
		　</c:forEach>
	　</table>	
</body>
</html>

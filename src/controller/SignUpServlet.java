package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;


@WebServlet(urlPatterns={"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branch = new BranchService().getBranch();
		request.setAttribute("branch",branch);

		List<Position> position = new PositionService().getPosition();
		request.setAttribute("position",position);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();
		HttpSession session = request.getSession();

		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setconfirmPass(request.getParameter("confirmPass"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setPosition(Integer.parseInt(request.getParameter("position")));

		if (isValid(request, messages) == true) {

			new UserService().register(user);

			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("branch",branch);
			request.setAttribute("position",position);
			request.setAttribute("inputUser",user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmPass = request.getParameter("confirmPass");
		String name = request.getParameter("name");	

		if (loginId.isEmpty() == true) {
			messages.add("IDを入力してください");	
		} else {

			if (!loginId.matches("^[a-zA-Z0-9]{6,20}$")) {
				messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
			}
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else {
			if (!password.matches("^[!-~]{6,20}$")) {
				messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
			}
			if (!confirmPass.equals(password)) {
				messages.add("パスワードが一致しません");
			}
		}


		if (name.length() > 10) {
			messages.add("氏名を10文字以下で入力してください。");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}



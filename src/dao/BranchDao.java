package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import beans.Branch;
import exception.SQLRuntimeException;
import static utils.CloseableUtil.*;

public class BranchDao {

	public List<Branch> getBranch(Connection connection) {

		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM branch";

			ps =connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Branch>ret = toBranchList(rs);

			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs)
			throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				String branchName = rs.getString("name");
				int id = rs.getInt("id");

				Branch branch = new Branch();
				branch.setName(branchName);
				branch.setId(id);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}




package dao;

import static utils.CloseableUtil.close;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> select(Connection connection) {

		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM position";

			ps =connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Position>ret = toPositionList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Position> toPositionList(ResultSet rs)
			throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				String positionName = rs.getString("name");
				int id = rs.getInt("id");

				Position position = new Position();
				position.setName(positionName);
				position.setId(id);

				ret.add(position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}



package dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import beans.User;
import java.sql.Connection;
import exception.SQLRuntimeException;
import static utils.CloseableUtil.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import exception.NoRowsUpdatedRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); 
			sql.append(", ?"); 
			sql.append(", ?"); 
			sql.append(", ?"); 
			sql.append(", ?"); 
			sql.append(", CURRENT_TIMESTAMP"); 
			sql.append(", CURRENT_TIMESTAMP"); 
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<User> getUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as loginId, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch as branch, ");
			sql.append("users.position as position, ");
			sql.append("branch.name as branch_name, ");
			sql.append("position.name as position_name, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date  ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch = branch.id ");
			sql.append("INNER JOIN position ");
			sql.append("ON users.position = position.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("loginId");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int position = rs.getInt("position");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setBranch(branch);
				user.setPosition(position);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);
				user.setBranch_name(branch_name);
				user.setPosition_name(position_name);

				ret.add(user);
			}
			return ret;

		} finally {
			close(rs);
		}
	}
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			if (StringUtils.isEmpty(user.getPassword())==false) {
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getPosition());
			if (StringUtils.isEmpty(user.getPassword())==false) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User editUser(Connection connection, int setId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as loginId, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch as branch, ");
			sql.append("users.position as position, ");
			sql.append("branch.name as branch_name, ");
			sql.append("position.name as position_name, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date  ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch = branch.id ");
			sql.append("INNER JOIN position ");
			sql.append("ON users.position = position.id ");
			sql.append(" WHERE users.id = ? ");


			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1,setId);

			ResultSet rs = ps.executeQuery();
			List<User> ret =  toUserList(rs);

			if (ret.isEmpty() == true) {
				return null;
			} else if (2 <= ret.size()) {
				throw new IllegalStateException("2 <= ret.size()");
			} else {
				return ret.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
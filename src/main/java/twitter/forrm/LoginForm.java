package twitter.forrm;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class LoginForm {
	@NotEmpty(message = "※アカウント名を入力してください")
	private String account;
	@NotEmpty(message = "※パスワードが空です")
	@Length(min=6,message ="※パスワードを6文字以上で入力してください")
	private String password;

	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}

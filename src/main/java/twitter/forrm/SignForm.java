package twitter.forrm;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class SignForm {
	private int id;
	@NotEmpty(message = "※名前を入力してください")
	private String name;
	@NotEmpty(message = "※アカウント名を入力してください")
	private String account;
	@NotEmpty(message = "※パスワードが空です")
	@Length(min=6,message ="※パスワードを6文字以上で入力してください")
	private String password;
	@NotEmpty(message = "※メールアドレスを入力してください")
	private String email;
//	private Date createdDate;
//	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
//	public Date getCreatedDate() {
//		return createdDate;
//	}
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//	public Date getUpdatedDate() {
//		return updatedDate;
//	}
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}
}

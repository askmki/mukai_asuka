<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<html>
<head>
<meta charset="utf-8">
<title>Welcome Twitter</title>
</head>
<body>
	<h2>${message}</h2>
	<div class="main-contents">
		<div class="header">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</div>
</body>
</html>

<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${title}"></c:out></title>
</head>
<body>
	<h1>
		<c:out value="${title}"></c:out>
	</h1>
	<p>
		<c:out value="${message}"></c:out>
	</p>
	<form:form modelAttribute="signForm">
	<div><form:errors path="*"  /></div>
		<table>
			<tbody>
				<tr>
					<td><form:label path="name">名前：</form:label></td>
					<td><form:input path="name" size="20" /></td>
				</tr>
				<tr>
					<td><form:label path="account">アカウント名：</form:label></td>
					<td><form:input path="account" size="20" /></td>
				</tr>
				<tr>
					<td><form:label path="password">パスワード：</form:label></td>
					<td><form:input path="password" cols="20" /></td>
				</tr>
				<tr>
					<td><form:label path="email">メールアドレス：</form:label></td>
					<td><form:input path="email" cols="20" /></td>
				</tr>

			</tbody>
		</table>
		<input type="submit" value="★登録★" />
		<br />
		<a href="./">戻る</a>
	</form:form>
</body>
</html>
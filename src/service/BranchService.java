package service;

import beans.Branch;
import dao.BranchDao;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;
import java.sql.Connection;
import java.util.List;

public class BranchService {
	public List<Branch> getBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranch(connection);

			commit(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}